![captura de pantalla 2018-04-14 a las 5 36 52 p m](https://user-images.githubusercontent.com/32823481/38769888-7f244cea-400a-11e8-80a7-293dc415c086.png)  

# Our principles

This three principles are our basis for achieving a project that is sustainable over time and that allows us to help contribute to a free and secure Internet for all those who want to be part of it.

__a) Opennes:__  
Our project is thought as a open network of collaborators where they can work, give ideas, make improvements, take decisions, and so on.

That is why we are working with open repositories to crate a community that works with us together to:

- promote ideas, projects, activist to interview, etc.  

- design and improve our website

- create, improve and distribute content  
_for example, submit written interviews, documentary films, photos, animations, etc_

- create, improve and use toolkits:  
_to facilitate the production of our content  we are creating tools and templates to ease and standardize workflows in a privacy by default multilingual platform._

- translate our content to multiple languages  
_Our content as well as our community building effort targets non-english speaking people, too. That's why offering our platform in multiple languages is crucial to us_

- promote the re-utilization and remix of content:  
_BA works with Licenses that allow anyone to share and adapt our content (CC BY 4.0 for content/ MIT for code)_

__b) Decentralization:__  

Our project is thought as a global community of passionate Internet users who share our vision. They can work in our platform where we are going to use open standards to facilitate the interoperability. 

__c) Digital Inclusion:__  
Beyond Activismo is a platform that promotes diversity and respect. We strongly believe that only by bringing different perspectives and experiences we'll be able to achieve our goals. To crate a safe and accessible collaboration platform is at the core of our development.  
